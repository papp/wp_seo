<?
class wp_seo__admin__seourl extends wp_seo__admin__seourl__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			case 'set_url':
				$this->C->seo()->set_url();
				exit;
				break;
		}
		
		$this->C->seo()->get_url();
	}
	
	function show($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display(__dir__.'/tpl/admin__seourl.tpl');
	}
}