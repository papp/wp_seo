<?
class wp_seo__class__seo extends wp_seo__class__seo__parent
{
	function get_url()
	{
		$D = &$this->D['MODUL']['D']['wp_seo'];
		$W = ($D['SEO']['W'])? " AND {$D['SEO']['W']}":'';

		$qry = $this->C->db()->query("SELECT id,active,seo_url_md5,seo_url,url,utimestamp,itimestamp FROM seo_url WHERE 1 {$W}");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$D['SEO']['D'][ $ary['id'] ] = array(
				'ACTIVE'		=> $ary['active'],
				'SEO_URL'		=> $ary['seo_url'],
				'URL'			=> $ary['url'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
			);
			$D['SEO']['MAP'][ $ary['seo_url_md5'] ] = $ary['id'];
		}
	}
	
	function set_url()
	{
		$D = &$this->D['MODUL']['D']['wp_seo'];
		foreach((array)$D['SEO']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -1)
			{
				$IU .= (($IU)?',':'')."('{$k}'";
				$IU .= (isset($v['ACTIVE']))?", '{$v['ACTIVE']}'":", NULL";
				$IU .= (isset($v['SEO_URL']))?", MD5('{$v['SEO_URL']}')":", NULL";
				$IU .= (isset($v['SEO_URL']))?", '{$v['SEO_URL']}'":", NULL";
				$IU .= (isset($v['URL']))?", '{$v['URL']}'":", NULL";
				$IU .= ")";
			}
			else
			{
				$DEL .= ($DEL?',':'')."'{$k}'";
			}
		}
		
		if($IU)#ToDo: Insert Update; chanel_id
			$this->C->db()->query("INSERT INTO seo_url (id,active,seo_url_md5,seo_url,url) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE seo_url.active END,
									seo_url_md5 = CASE WHEN VALUES(seo_url) IS NOT NULL THEN MD5(VALUES(seo_url)) ELSE seo_url.seo_url_md5 END,
									seo_url = CASE WHEN VALUES(seo_url) IS NOT NULL THEN VALUES(seo_url) ELSE seo_url.seo_url END,
									url = CASE WHEN VALUES(url) IS NOT NULL THEN VALUES(url) ELSE seo_url.url END
									");
		if($DEL)
			$this->C->db()->query("DELETE FROM seo_url WHERE id IN ({$DEL})");
	}
}