<?
class wp_seo__20170101000000_install
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	
	function up()
	{
		$this->C->db()->query("CREATE TABLE `seo_url` (
  `id` varchar(32) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `seo_url_md5` varchar(32) NOT NULL,
  `seo_url` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `utimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `itimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("ALTER TABLE `seo_url`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `seo_url_md5` (`seo_url_md5`) USING BTREE;");
		return 1;
	}
	
	function down()
	{
		return 1;
	}
}