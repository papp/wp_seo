<form id="formSEO">
	
		<div class="tab-content" id="myTabContent">
			<ul class="nav nav-tabs" id="myTabs">
				<li class="active"><a href="#tab_0">200 OK</a></li>
				<li><a href="#tab_1">301 Moved Permanently</a></li>
				<li><a href="#tab_2">404 Not Found</a></li>
			</ul>
			
				<div class="panel-body tab-pane fade in active" role="tabpanel" id="tab_0" aria-labelledby="home-tab">
					<table class="table">
						<thead>
							<th>aktive</th>
							<th>von URL</th>
							<th>zu URL</th>
							<th>del</th>
						</thead>
						<tbody>
							{foreach from=$D.MODUL.D['wp_seo'].SEO.D name=SEO item=SEO key=kSEO}
								{if $SEO.URL != '' AND $SEO.URL|strstr:"D[PAGE]"}
							<tr id="trSEO{$kSEO}">
								<td><input id="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]" type="hidden" value="{$SEO.ACTIVE}">
									<input type="checkbox" {if $SEO.ACTIVE}checked{/if} onclick="document.getElementById('D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]').value = this.checked?'1':'0'">
								</td>
								<td><div class="input-group input-group-sm"><span class="input-group-addon">/</span><input value="{$SEO.SEO_URL}" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][SEO_URL]" class="form-control"></div></td>
								<td><div class="input-group input-group-sm"><span class="input-group-addon" >/</span><input value="{$SEO.URL}" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][URL]" class="form-control"></div></td>
								<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]').value = '-1';document.getElementById('trSEO{$kSEO}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
							</tr>
								{/if}
							{/foreach}
						</tbody>
					</table>
				</div>
				<div class="panel-body tab-pane fade in" role="tabpanel" id="tab_1">
					<table class="table">
						<thead>
							<th>aktive</th>
							<th>von URL</th>
							<th>zu URL</th>
							<th>del</th>
						</thead>
						<tbody>
							{foreach from=$D.MODUL.D['wp_seo'].SEO.D name=SEO item=SEO key=kSEO}
								{if $SEO.URL != '' AND !$SEO.URL|strstr:"D[PAGE]"}
							<tr id="trSEO{$kSEO}">
								<td><input id="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]" type="hidden" value="{$SEO.ACTIVE}">
									<input type="checkbox" {if $SEO.ACTIVE}checked{/if} onclick="document.getElementById('D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]').value = this.checked?'1':'0'">
								</td>
								<td><div class="input-group input-group-sm"><span class="input-group-addon">/</span><input value="{$SEO.SEO_URL}" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][SEO_URL]" class="form-control"></div></td>
								<td><div class="input-group input-group-sm"><span class="input-group-addon">/</span><input value="{$SEO.URL}" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][URL]" class="form-control"></div></td>
								<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]').value = '-1';document.getElementById('trSEO{$kSEO}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
							</tr>
								{/if}
							{/foreach}
						</tbody>
					</table>
				</div>
				<div class="panel-body tab-pane fade in" role="tabpanel" id="tab_2">
					<table class="table">
						<thead>
							<th>aktive</th>
							<th>von URL</th>
							<th>zu URL</th>
							<th>del</th>
						</thead>
						<tbody>
							{foreach from=$D.MODUL.D['wp_seo'].SEO.D name=SEO item=SEO key=kSEO}
								{if $SEO.URL == ''}
							<tr id="trSEO{$kSEO}">
								<td><input id="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]" type="hidden" value="{$SEO.ACTIVE}">
									<input type="checkbox" {if $SEO.ACTIVE}checked{/if} onclick="document.getElementById('D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]').value = this.checked?'1':'0'">
								</td>
								<td><div class="input-group input-group-sm"><span class="input-group-addon">/</span><input value="{$SEO.SEO_URL}" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][SEO_URL]" class="form-control"></div></td>
								<td><div class="input-group input-group-sm"><span class="input-group-addon">/</span><input value="{$SEO.URL}" name="D[MODUL][D][wp_seo][SEO][D][{$kSEO}][URL]" class="form-control"></div></td>
								<td><button class="btn btn-danger btn-xs" type="button" onclick="document.getElementById('D[MODUL][D][wp_seo][SEO][D][{$kSEO}][ACTIVE]').value = '-1';document.getElementById('trSEO{$kSEO}').style.display = 'none';"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
							</tr>
								{/if}
							{/foreach}
						</tbody>
					</table>
				</div>
				
				
			
		</div>
		<div class="panel-footer text-right">
			<div class="btn-group">
				<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__seourl&D[ACTION]=set_url', data : $('#formSEO').serialize() /*, success: function(data) { $('#page').load('?D[PAGE]=admin__seourl');}*/ });" class="btn btn-default">Speichern</button>
				<button type="button" onclick="$('#page').load('?D[PAGE]=admin__seourl'); return false;" class="btn btn-default">Abbrechen</button>
			</div>
		</div>
	
</form>
<script>
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});
</script>