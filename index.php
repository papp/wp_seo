<?
class wp_seo__index extends wp_seo__index__parent
{
	function load($d=null)
	{
		parent::{__function__}();
		$this->D['PAGE'] = null;
		$this->D['MODUL']['D']['wp_seo']['SEO']['W'] = "SEO_URL_MD5 = '".md5($this->D['SEO_URL'])."' AND ACTIVE = 1";
		$this->C->seo()->get_url();
		$ID = $this->D['MODUL']['D']['wp_seo']['SEO']['MAP'][ md5($this->D['SEO_URL']) ];
		if($url = $this->D['MODUL']['D']['wp_seo']['SEO']['D'][ $ID ]['URL']) #Link in der DB gefunden
		{
			parse_str($url, $d);
			$this->D = array_merge($this->D,(array)$d['D']);
			if(!$this->D['PAGE']) #Link beinhaltet kein D[PAGE] dann ist das ein SEO URL #ToDo: Trotz SEO URL wird D.PAGE gefüllt
			{
				if($this->D['MODUL']['D']['wp_seo']['SEO']['D'][ $ID ]['URL'] != '' && strpos($this->D['MODUL']['D']['wp_seo']['SEO']['D'][ $ID ]['URL'],'D[PAGE]') !== false)
					header( "HTTP/1.1 200 OK" );
				elseif($this->D['MODUL']['D']['wp_seo']['SEO']['D'][ $ID ]['URL'] != '')
					header( "HTTP/1.1 301 Moved Permanently" );
				else
					header( "HTTP/1.1 404 Not Found" );
					

				header("Location: {$this->D['MODUL']['D']['wp_seo']['SEO']['D'][ $ID ]['URL']}");
				exit;
			}
			if($this->D['SEO_URL'] == '404')
				header( "HTTP/1.1 404 Not Found" );
		}
		else #Link nicht vorhanden
		{
			if(!strpos($this->D['SEO_URL'],'seourl'))
			{
				$this->D['MODUL']['D']['wp_seo']['SEO']['D'][md5($this->D['SEO_URL'])]['SEO_URL'] = $this->D['SEO_URL'];
				$this->C->seo()->set_url();
			}
			header( "HTTP/1.1 404 Not Found" );
			#$this->D['PAGE'] = 'index';
		}
		if($this->D['PAGE'])
		{
			include_once("tmp/system/{$this->D['PAGE']}.php");
			$p = new $this->D['PAGE']();
			$p->load();
			$p->show();
			#(new $this->D['PAGE']())->load();
			exit;
		}
		else
		{
			header("Location: 404");
			exit('404');
		}
	}
}